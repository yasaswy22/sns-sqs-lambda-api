terraform {
  cloud {
    organization = "yasaswy-tc"

    workspaces {
      name = "tf-push_not-test"
    }
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.28.0"
    }
  }

  required_version = ">= 0.14.0"
}

provider "aws" {
  region = var.region
}

# Terrafrom cloud help
# https://developer.hashicorp.com/terraform/tutorials/cloud-get-started/cloud-login

# https://www.youtube.com/live/m3PlM4erixY?feature=share
# https://youtu.be/zARP_Cu8dwQ