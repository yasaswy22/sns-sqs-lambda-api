module "sns" {
  source      = "./modules/sns"
  name_prefix = "tf"
  env         = "test"
  sqs_arn     = module.sqs.tf_push_notification_sqs_arn
}

module "sqs" {
  source                 = "./modules/sqs"
  name_prefix            = "tf"
  env                    = "test"
  visibility_timeout_sec = 60
  message_retention_sec  = 60
  delay_sec              = 0
  max_message_size       = 262144
  sns_arn                = module.sns.tf_push_notification_sns_arn
  tags                   = var.global_tags
}

module "lambda" {
  source                   = "./modules/lambda"
  lambda_function_location = "modules/lambda/lambda_function"
  # lambda_function_location = "modules/lambda"
  lambda_file_name         = "push-notification-processor"
  log_group_name           = "push-notification-processor"
  selected_runtime_env     = var.runtime_main["python38"]
  timeout                  = 60
  sqs_arn                  = module.sqs.tf_push_notification_sqs_arn
  tags                     = var.global_tags
}

# Tutorial:
# SNS, SQS: https://www.youtube.com/watch?v=e7P0TGwp1VA&ab_channel=CloudQuickLabs
# SNS, SQS, Lambda: https://youtu.be/tTD5D9ZHYUc