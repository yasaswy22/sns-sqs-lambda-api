output "tf_push_notification_sqs_arn" {
  value = aws_sqs_queue.tf_push_notification_sqs.arn
}