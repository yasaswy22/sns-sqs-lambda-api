variable "name_prefix" {
  type = string
}

variable "env" {
  type = string
}

variable "visibility_timeout_sec" {
  type = string
}

variable "message_retention_sec" {
  type = string
}

variable "delay_sec" {
  type = string
}

variable "max_message_size" {
  type = string
}

variable "sns_arn" {
  type = string
}

variable "tags" {
  type = map(string)
}

