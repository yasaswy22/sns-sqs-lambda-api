resource "aws_sqs_queue" "tf_push_notification_sqs" {
  name                       = "${var.name_prefix}-${var.env}-push-notification-sqs"
  visibility_timeout_seconds = var.visibility_timeout_sec
  message_retention_seconds  = var.message_retention_sec
  delay_seconds              = var.delay_sec
  max_message_size           = var.max_message_size
  tags                       = var.tags
}

resource "aws_sqs_queue_policy" "tf_push_notification_sqs_policy" {
  queue_url = aws_sqs_queue.tf_push_notification_sqs.id
  policy    = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "message-sender-policy",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "sqs:SendMessage",
            "Resource": "${aws_sqs_queue.tf_push_notification_sqs.arn}",
            "Condition": {
                "ArnEquals": {
                    "aws:SourceArn": "${var.sns_arn}"
                }
            }
        }
    ]
}
POLICY
}