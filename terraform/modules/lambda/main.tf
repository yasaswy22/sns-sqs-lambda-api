# Lambda structure
# terraform/
#     lambda/
#         lambda_function/
#             __init__.py
#             push-notification-processor.py
#             requirements.txt
#         scripts/
#             create_pkg.sh
#         main.tf
#         variables.tf
#         outputs.tf

# When to run the provisioner
# https://stackoverflow.com/questions/71759028/local-exec-not-detecting-the-lambda-source-code-changes
# how to run the provisioner tutorial
# https://alek-cora-glez.medium.com/deploying-aws-lambda-function-with-terraform-custom-dependencies-7874407cd4fc

resource "null_resource" "install_python_dependencies" {

  triggers = {
     always_run = timestamp()
   }

  provisioner "local-exec" {
    command = "bash ${path.module}/scripts/create_pkg.sh"

    environment = {
      source_code_path = var.path_source_code
      function_name    = var.lambda_file_name
      path_module      = path.module
      runtime          = var.selected_runtime_env
      path_cwd         = path.cwd
    }
  }
}

data "archive_file" "create_dist_pkg" {
  depends_on  = [null_resource.install_python_dependencies]
  source_dir  = "${path.cwd}/lambda_dist_pkg/"
  output_path = var.output_path
  type        = "zip"
}

data "archive_file" "lambda_zipper" {
  type        = "zip"
  source_file = "${var.lambda_function_location}/${var.lambda_file_name}.py"
  output_path = "${var.lambda_function_location}/${var.lambda_file_name}.zip"
}

resource "aws_iam_role" "assume_role_lambda" {
  name               = "assume_role_for_lambda"
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY 
}

# Note: the trailing space before JSON for good indentation gives policy error below.
# "policy" contains an invalid JSON policy
# Make sure they touch the left border
resource "aws_iam_policy" "lamda_role_cloudwatch_logs_policy" {
  name   = "lambda_logs_policy"
  path   = "/"
  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "lambdaloggrouppolicystatement",
            "Effect": "Allow",
            "Action": "logs:CreateLogGroup",
            "Resource": "arn:aws:logs:us-east-1:912589285779:*"
        },
        {
            "Sid": "lambdalogstreampolicystatement",
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": [
                "arn:aws:logs:us-east-1:912589285779:log-group:/aws/lambda/${var.log_group_name}:*"
            ]
        }
    ]
}
POLICY
}

resource "aws_iam_policy" "lamda_role_sqs_policy" {
  name   = "lambda_sqs_policy"
  path   = "/"
  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "lambdasqspolicystatement",
            "Effect": "Allow",
            "Action": [
                "sqs:ChangeMessageVisibility",
                "sqs:DeleteMessage",
                "sqs:GetQueueAttributes",
                "sqs:ReceiveMessage"
            ],
            "Resource": "${var.sqs_arn}"
        }
    ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "lambda_sqs_policy_role_attached" {
  role       = aws_iam_role.assume_role_lambda.name
  policy_arn = aws_iam_policy.lamda_role_sqs_policy.arn
}

resource "aws_iam_role_policy_attachment" "lambda_logs_policy_role_attached" {
  role       = aws_iam_role.assume_role_lambda.name
  policy_arn = aws_iam_policy.lamda_role_cloudwatch_logs_policy.arn
}

resource "aws_lambda_layer_version" "lambda_layer" {
  depends_on  = [null_resource.install_python_dependencies]
  source_code_hash = data.archive_file.create_dist_pkg.output_base64sha256
  filename      = data.archive_file.create_dist_pkg.output_path
  layer_name = "tf_python_dependencies"
  
  compatible_runtimes = [var.selected_runtime_env]
}

resource "aws_lambda_function" "lambda_function" {

  # depends_on  = [null_resource.install_python_dependencies]
  description = "Make the API calls based on the notification event."

  source_code_hash = data.archive_file.lambda_zipper.output_base64sha256
  # source_code_hash = data.archive_file.create_dist_pkg.output_base64sha256
  filename         = "${var.lambda_function_location}/${var.lambda_file_name}.zip"
  # filename      = data.archive_file.create_dist_pkg.output_path
  function_name = var.lambda_file_name
  handler          = "${var.lambda_file_name}.lambda_handler"
  # handler = "lambda_function.push-notification-processor.lambda_handler"
  role    = aws_iam_role.assume_role_lambda.arn
  runtime = var.selected_runtime_env
  timeout = var.timeout

  layers = [aws_lambda_layer_version.lambda_layer.arn]

  tags = var.tags
}

resource "aws_lambda_event_source_mapping" "sqs_lambda_trigger" {
  event_source_arn = var.sqs_arn
  function_name    = aws_lambda_function.lambda_function.arn
}

