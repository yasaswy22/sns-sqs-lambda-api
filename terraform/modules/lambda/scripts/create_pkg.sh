#!/bin/bash

echo "Executing create_pkg.sh..."
echo "path_cwd: "$path_cwd
cd $path_cwd
python3 --version
dir_name=lambda_dist_pkg/python
mkdir -p $dir_name

python3 -m pip install --upgrade pip
pip3 install virtualenv

# Create and activate virtual environment...
virtualenv -p $runtime env_$function_name
source $path_cwd/env_$function_name/bin/activate

echo -e "\n--------------------------------------"
echo "Find the working directory structure.."
ls
echo -e "--------------------------------------\n"

# Installing python dependencies...
FILE=$path_cwd/modules/lambda/lambda_function/requirements.txt

if [ -f "$FILE" ]; then
  echo "Installing dependencies..."
  echo "From: requirement.txt file exists..."
  pip install -r "$FILE"

else
  echo "Error: requirement.txt does not exist!"
fi

# Deactivate virtual environment...
deactivate

# Create deployment package...
echo "Creating deployment package..."

echo "cd env_"$function_name"/lib/"$runtime"/site-packages/"
cd env_$function_name/lib/$runtime/site-packages/
echo -e "--------------------------------------"
echo "Find the working directory structure.."
ls
echo -e "--------------------------------------\n"

echo "cp -r . "$path_cwd"/"$dir_name
echo -e "Copying all the downloaded dependencies to "$path_cwd"/"$dir_name"\n"
cp -r . $path_cwd/$dir_name

echo "Final packaged files to be archived to Lambda archiver"
echo "cd "$path_cwd"/"$dir_name
cd $path_cwd/$dir_name
echo -e "--------------------------------------"
echo "Find the working directory structure.."
ls
echo -e "--------------------------------------\n"

# Removing virtual environment folder...
echo "Removing virtual environment folder..."
rm -rf $path_cwd/env_$function_name

echo "Finished script execution!"