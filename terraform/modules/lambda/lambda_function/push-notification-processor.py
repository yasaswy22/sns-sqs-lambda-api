import json
import logging
import requests
from datetime import datetime

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def lambda_handler(event,context):
    try:
        now = datetime.now()
        now_str = now.strftime("%Y-%m-%d %H:%M:%S")
        
        # Retrieve data from event body.

        # # 1. Parse out the query string params
        sns_body = json.loads(event["Records"][0]["body"])
        message_body = json.loads(sns_body["Message"])
        logging.info(f'Printing the Notification Category: {str(message_body["notification_category"])}')

        # 2. Construct the body of the response object
        notificationResponse = {}
        url = 'http://api.openweathermap.org/data/2.5/forecast?q=Washington,us&APPID=53d6430c1eccacb54e827045d1aee3d3'
        # headers = {"content-type": "application/json", "Authorization": "Basic Zxxxxxxxxx3NjxxZxxxxzcw==" }
        response_api = requests.get(url)
        notificationResponse['message'] = f'Response object: {str(response_api)}'
        
        # Check if the request completed successfully
        if response_api.status_code == 200:
            # Convert response data to python dictionary object
            datadictionary = response_api.json()
            logging.info(f'Response from Weather API: {datadictionary}')
        
        # Return the desired weather information for today from the dictionary object
        for data in datadictionary["list"]:
            logging.info(f'Latest Data record as of {data["dt_txt"]}, from the weather dictionary: {data}')
            if data["dt_txt"] > now_str:
        
                # Return data and print data
                description = data["weather"][0]["description"]
                humidity = data["main"]["humidity"]
                sea_level = data["main"]["sea_level"]
                temp = data["main"]["temp"]
                logging.info("weather:{}, humidity: {}, sea_level: {},temperature: {}".format(description,humidity,sea_level,temp))
                return "weather:{}, humidity: {}, sea_level: {},temperature: {}".format(description,humidity,sea_level,temp)


        # # 3. Construct the http response object
        # responseObject = {}
        # responseObject['statusCode'] = 200
        # responseObject['headers'] = {}
        # responseObject['headers']['Content-Type'] = 'application/json'
        # responseObject['body'] = json.dumps(notificationResponse)

        # logging.info(f'Logging the constructed response object formmed within the Lambda: {responseObject}')
        # return responseObject

    except Exception as e:
        logging.error("Unable to form the response object in Lambda", exc_info=True)
        return f' Error occured: {e}'
    
# Test request
# {"notification_category": "Crash Detect"}

# documentation to install requests dependencies for Lambda
# https://docs.aws.amazon.com/lambda/latest/dg/python-package.html