variable "lambda_function_location" {
  type = string
}

variable "lambda_file_name" {
  type = string
}

variable "log_group_name" {
  type = string
}

variable "selected_runtime_env" {
  type = string
}

variable "timeout" {
}

variable "sqs_arn" {
  type = string
}

variable "tags" {
  type = map(string)
}


# Lambda structure
# terraform/
#     lambda/
#         lambda_function/
#             __init__.py
#             push-notification-processor.py
#             requirements.txt
#         scripts/
#             create_pkg.sh
#         main.tf
#         variables.tf
#         outputs.tf


variable "path_source_code" {
  default = "lambda_function"
}

# variable "function_name" {
#   default = "aws_lambda_test"
# }

# variable "runtime" {
#   default = "python3.7"
# }

variable "output_path" {
  description = "Path to function's deployment package into local filesystem. eg: /path/lambda_function.zip"
  # default     = "lambda_function.zip"
  default     = "python.zip"
}

variable "distribution_pkg_folder" {
  description = "Folder name to create distribution files..."
  default     = "lambda_dist_pkg"
}