resource "aws_sns_topic" "tf_push_notification_sns" {
  name = "${var.name_prefix}-${var.env}-push-notification-sns-topic"
}

resource "aws_sns_topic_subscription" "tf_push_notification_sns_sub" {
  topic_arn = aws_sns_topic.tf_push_notification_sns.arn
  protocol  = "sqs"
  endpoint  = var.sqs_arn
}