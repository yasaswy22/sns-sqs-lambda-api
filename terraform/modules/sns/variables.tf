variable "name_prefix" {
  type = string
}

variable "env" {
  type = string
}

variable "sqs_arn" {
  type = string
}