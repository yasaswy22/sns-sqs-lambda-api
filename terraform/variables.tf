variable "global_tags" {
  description = "(Optional) A mapping of tags to assign to the bucket."
  type        = map(string)
  default = {
    environment = "test"
    name        = "terraform_prac"
  }
}

variable "runtime_main" {
  type = map(any)
  default = {
    "python38" = "python3.8"
    "python39" = "python3.9"
    "nodejs12" = "nodejs12.x"
  }
}

variable "region" {
  type    = string
  default = "us-east-1"
}